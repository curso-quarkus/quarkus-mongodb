package quarkus.mongodb;

import org.bson.types.ObjectId;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/produto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProdutoResource {

    @Inject
    ProdutoRepository produtoRepository;

    @GET
    public List<Produto> produtos() {
        return produtoRepository.listar();
    }

    @GET
    @Path("find/{nome}")
    public List<Produto> findByNomeLike(
            @PathParam String nome
    ) {
        return produtoRepository.findByNomeLike(nome);
    }

    @GET
    @Path("/{id}")
    public Produto getById(
            @PathParam String id
    ) {
        ObjectId objectId = new ObjectId(id);
        return produtoRepository.findById(objectId);
    }

    @POST
    @Transactional
    public Produto salvar(Produto produto) {
        produto.persist();
        return produto;
    }

    @PUT
    @Transactional
    public Produto editar(Produto produto) {
        produto.persistOrUpdate();
        return produto;
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public void deletar(@PathParam String id) {
        ObjectId objectId = new ObjectId(id);
        Produto produto = produtoRepository.findById(objectId);
        produto.delete();
    }
}