package quarkus.mongodb;

import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface CategoriaMapper {

    Categoria toEntity(CategoriaDTO categoriaDTO);

    CategoriaDTO toDTO(Categoria categoria);

}
