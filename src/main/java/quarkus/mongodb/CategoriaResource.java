package quarkus.mongodb;

import org.bson.types.ObjectId;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/categoria")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoriaResource {

    @Inject
    CategoriaRepository categoriaRepository;

    @Inject
    public CategoriaMapper categoriaMapper;

    @GET
    public List<Categoria> categorias() {
        return categoriaRepository.listar();
    }

    @GET
    @Path("find/{nome}")
    public List<Categoria> findByNomeLike(
            @PathParam String nome
    ) {
        return categoriaRepository.findByNomeLike(nome);
    }

    @GET
    @Path("/{id}")
    public Categoria getById(
            @PathParam String id
    ) {
        ObjectId objectId = new ObjectId(id);
        return categoriaRepository.findById(objectId);
    }

    @POST
    @Transactional
    public Categoria salvar(CategoriaDTO categoriaDTO) {
        Categoria categoria = categoriaMapper.toEntity(categoriaDTO);
        categoria.persist();
        return categoria;
    }

    @PUT
    @Transactional
    public Categoria editar(Categoria categoria) {
        categoria.persistOrUpdate();
        return categoria;
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public void deletar(@PathParam String id) {
        ObjectId objectId = new ObjectId(id);
        Categoria categoria = categoriaRepository.findById(objectId);
        categoria.delete();
    }
}