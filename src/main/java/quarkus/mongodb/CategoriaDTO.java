package quarkus.mongodb;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoriaDTO {
    private String nome;
}