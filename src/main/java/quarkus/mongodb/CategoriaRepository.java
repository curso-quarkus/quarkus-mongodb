package quarkus.mongodb;

import io.quarkus.mongodb.panache.PanacheMongoRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class CategoriaRepository implements PanacheMongoRepository<Categoria> {

    public List<Categoria> listar() {
        return listAll();
    }

    public List<Categoria> findByNomeLike(String nome) {
        return find("{'nome': {'$regex': ?1}}", nome).list();

    }

}
