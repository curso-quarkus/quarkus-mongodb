package quarkus.mongodb;

import io.quarkus.mongodb.panache.PanacheMongoRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class ProdutoRepository implements PanacheMongoRepository<Produto> {

    public List<Produto> listar() {
        return listAll();
    }

    public List<Produto> findByNomeLike(String nome) {
        return find("{'nome': {'$regex': ?1}}", nome).list();

    }

}
